		//addition
		function printSum(addend1, addend2){

		
			console.log('Displayed sum of '+ addend1+ ' and '+ addend2)
			console.log(addend1+addend2);
		}
		printSum(5, 15);

		//subtraction
		function printDifference(minuend, subtrahend){

			
			console.log('Displayed difference of '+ minuend+ ' and '+ subtrahend)
			console.log(minuend-subtrahend);
		}
		printDifference(20, 5);


		//multiplication
		function returnProduct(factor1, factor2){
		
			console.log( 'The product of '+ factor1+ ' and '+ factor2);
			let product= factor1 * factor2
			return product;
			}
			
			let printReturnProd= returnProduct(50, 10);
			console.log(printReturnProd);

			
		//division
		function returnQuotient(dividend, divisor){
		
			console.log( 'The quotient of '+ dividend+ ' and '+ divisor);
			let quotient= dividend / divisor
			return quotient;
			}
			let printReturnQuo= returnQuotient(50, 10);
			console.log( printReturnQuo);

		//area of circle
		const Π= 3.1416
	
		function returnArea(radius){
			console.log('The result of getting the area of a circle with '+radius +' radius:')
			let area=  Π * (radius**2);
			return area
			}
			let printReturnArea= returnArea(15);
			console.log(printReturnArea);



		//average
		function returnAverage(num1, num2, num3, num4){
			console.log('The average of '+num1+' '+num2+' '+num3+' and '+num4+':')
			let averageVar= (num1+ num2+ num3+ num4) / 4
			return averageVar
		}
		let printReturnAverage= returnAverage(20, 40, 60, 80);
		console.log(printReturnAverage);


		//pass or fail
		function returnPassOrFail(userScore, TotalScore){
			console.log('Is '+userScore+'/'+TotalScore+' a passing score?')
			let isPassOrFail= (userScore/TotalScore) * 100 >= 75
			return isPassOrFail


		}

		let printReturnPassOrFail= returnPassOrFail(38,50);
		console.log(printReturnPassOrFail);
